FROM centos:7

ADD requirements.txt requirements.txt
RUN yum install python3 python3-pip -y && \
    pip3 install -r requirements.txt && \
    mkdir /app
ADD app.py /app/app.py
CMD ["python3","/app/app.py"]
